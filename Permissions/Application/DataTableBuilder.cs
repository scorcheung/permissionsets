﻿using System.Data;

namespace Permissions.Application
{
    internal static class DataTableBuilder
    {
        /// <summary>
        /// 查询符合条件的数据。
        /// </summary>
        /// <param name="dataTable">原始DataTable</param>
        /// <param name="where">查询条件</param>
        public static DataTable Select(DataTable dataTable, string where)
        {
            DataRow[] rows = dataTable.Select(where);
            int length = rows.Length;
            DataTable tableProcessed = dataTable.Clone();
            for (int i = 0; i < length; i++)
            {
                DataRow row = tableProcessed.NewRow();
                row.ItemArray = rows[i].ItemArray;
                tableProcessed.Rows.Add(row);
            }
            return tableProcessed;
        }

        /// <summary>
        /// 查询符合条件的前几条数据。
        /// </summary>
        /// <param name="dataTable">原始DataTable</param>
        /// <param name="where">查询条件</param>
        /// <param name="top">查询前几条数据</param>
        public static DataTable SelectByTop(DataTable dataTable, string where, int top)
        {
            return DataTableBuilder.SelectByTop(dataTable, where, 0, top);
        }

        /// <summary>
        /// 查询符合条件的前几条数据。
        /// </summary>
        /// <param name="dataTable">原始DataTable</param>
        /// <param name="where">查询条件</param>
        /// <param name="top">查询前几条数据</param>
        public static DataTable SelectByTop(DataTable dataTable, string where, int top, string sort)
        {
            return DataTableBuilder.SelectByTop(dataTable, where, 0, top, sort);
        }

        /// <summary>
        /// 获取指定索引间的数据。
        /// </summary>
        /// <param name="dataTable">原始DataTable</param>
        /// <param name="where">查询条件</param>
        /// <param name="startIndex">获取数据的开始索引</param>
        /// <param name="rowCount">获取数据的行数</param>
        public static DataTable SelectByTop(DataTable dataTable, string where, int startIndex, int rowCount)
        {
            return DataTableBuilder.SelectByTop(dataTable, where, startIndex, rowCount, null);
        }

        /// <summary>
        /// 获取指定索引间的数据。
        /// </summary>
        /// <param name="dataTable">原始DataTable</param>
        /// <param name="where">查询条件</param>
        /// <param name="startIndex">获取数据的开始索引</param>
        /// <param name="rowCount">获取数据的行数</param>
        /// <param name="sort">排序字段</param>
        public static DataTable SelectByTop(DataTable dataTable, string where, int startIndex, int rowCount, string sort)
        {
            DataRow[] rows;
            if (string.IsNullOrWhiteSpace(sort))
            {
                rows = dataTable.Select(where);
            }
            else
            {
                rows = dataTable.Select(where, sort);
            }
            DataTable tableProcessed = dataTable.Clone();
            int length = rows.Length;
            if (length > startIndex)
            {
                if (length > rowCount)
                {
                    length = rowCount;
                }
                for (int i = startIndex; i < length; i++)
                {
                    DataRow row = tableProcessed.NewRow();
                    row.ItemArray = rows[i].ItemArray;
                    tableProcessed.Rows.Add(row);
                }
            }
            return tableProcessed;
        }
    }
}