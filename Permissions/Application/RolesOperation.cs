﻿using System;
using System.Collections.Generic;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Database.Tables;
using System.Linq;
using Permissions.Database.StoredProcedures;
using Permissions.Database.Views;

namespace Permissions.Application
{
    /// <summary>
    /// 角色操作类
    /// </summary>
    internal static class RolesOperation
    {
        /// <summary>
        /// 检查当前角色与要继承的角色的关系
        /// </summary>
        /// <param name="original">当前角色ID</param>
        /// <param name="inherit">要继承的角色ID</param>
        public static BindingRelationship CheckUpWhetherCanInherit(String originalRoleID, String inheritRoleID)
        {
            //判断是否为继承关系
            if (RoleBindings.Exists(SqlWhereBuilder.MSSQL.Append("RoleID", originalRoleID).And("ParentRoleID", inheritRoleID)))
            {
                return BindingRelationship.Inherit;
            }
            //判断是否为被继承关系
            if (RoleBindings.Exists(SqlWhereBuilder.MSSQL.Append("RoleID", inheritRoleID).And("ParentRoleID", originalRoleID)))
            {
                return BindingRelationship.BeInherited;
            }
            //判断是否为隔代继承关系
            var roleParentList = GetAllParentRolesPro(originalRoleID);
            foreach (var item in roleParentList)
            {
                if (item.RoleID == inheritRoleID)
                {
                    return BindingRelationship.SkipGenerationInherit;
                }
            }

            //判断是否为隔代被继承关系
            var roleChildList = GetAllParentRolesPro(inheritRoleID);
            foreach (var item in roleChildList)
            {
                if (item.RoleID == originalRoleID)
                {
                    return BindingRelationship.SkipGenerationBeInherit;
                }
            }
            return BindingRelationship.CanInherit;
        }

        /// <summary>
        /// 获取所有继承角色（获取所有的子孙角色）
        /// </summary>
        /// <param name="parentRoleId"></param>
        /// <returns></returns>
        public static List<string> GetAllChildRoles(string parentRoleId)
        {
            childRoles.Clear();
            GetAllChildRolesRec(parentRoleId);
            return childRoles;
        }

        private static List<string> childRoles = new List<string>();
        /// <summary>
        /// 获取所有继承角色（获取所有的子孙角色）- 递归
        /// </summary>
        /// <param name="parentRoleId">当前角色ID</param>
        private static void GetAllChildRolesRec(String parentRoleId)
        {
            var roles = RoleBindings.Select(SqlWhereBuilder.MSSQL.Append("ParentRoleID", parentRoleId));
            foreach (var role in roles)
            {
                childRoles.Add(role.RoleID);
                GetAllChildRolesRec(role.RoleID);
            }
        }

        /// <summary>
        /// 获取所有继承角色（获取所有的子孙角色）- 存储过程
        /// </summary>
        /// <param name="parentRoleId">当前角色ID</param>
        public static IEnumerable<S_RolesInfo> GetAllChildRolesPro(String parentRoleId)
        {
            SP_GetAllChildRoles getAllChildRoles = SP_GetAllChildRoles.New;
            getAllChildRoles.ParentRoleID = parentRoleId;
            return getAllChildRoles.Select<S_RolesInfo>();
        }

        /// <summary>
        /// 获取所有父角色（获取所有的父角色）- 存储过程
        /// </summary>
        /// <param name="roleId">当前角色ID</param>
        public static IEnumerable<S_RolesInfo> GetAllParentRolesPro(String roleId)
        {
            SP_GetAllParentRoles getAllParentRoles = SP_GetAllParentRoles.New;
            getAllParentRoles.RoleID = roleId;
            return getAllParentRoles.Select<S_RolesInfo>();
        }

        /// <summary>
        /// 继承指定的角色
        /// </summary>
        /// <param name="rid">当前角色ID</param>
        /// <param name="pid">被继承角色ID</param>
        public static Boolean Inherit(String rid, String pid)
        {
            try
            {
                RoleBindings roleBindings = RoleBindings.New;
                roleBindings.RoleID = rid;
                roleBindings.ParentRoleID = pid;
                roleBindings.Insert();
                return true;
            }
            catch { }
            return false;
        }

        /// <summary>
        /// 取消继承指定的角色
        /// </summary>
        /// <param name="rid">当前角色ID</param>
        /// <param name="pid">被继承角色ID</param>
        public static Boolean CancelInherit(String rid, String pid)
        {
            RoleBindings roleBindings = RoleBindings.New;
            roleBindings.RoleID = rid;
            roleBindings.ParentRoleID = pid;
            return roleBindings.Delete() > 0;
        }
    }

}
