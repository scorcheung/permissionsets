﻿using System;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Database.Tables;

namespace Permissions.Application
{
    /// <summary>
    /// 用户操作类
    /// </summary>
    internal static class UsersOperation
    {
        /// <summary>
        /// 检查要继承的角色是否可以被继承
        /// </summary>
        /// <param name="original">当前用户ID</param>
        /// <param name="inherit">要继承的角色ID</param>
        public static BindingRelationship CheckUpWhetherCanInherit(String userID, String roleID)
        {
            if (UserBindings.Exists(SqlWhereBuilder.MSSQL.Append("UserID", userID).And("RoleID", roleID)))
            {
                return BindingRelationship.Inherit;
            }
            else
            {
                return BindingRelationship.CanInherit; 
            }
        }

        /// <summary>
        /// 继承指定的角色
        /// </summary>
        /// <param name="uid">当前用户ID</param>
        /// <param name="rid">被继承的角色ID</param>
        public static Boolean Inherit(String uid, String rid)
        {
            try
            {
                UserBindings userBindings = UserBindings.New;
                userBindings.UserID = uid;
                userBindings.RoleID = rid;
                userBindings.Insert();
                return true;
            }
            catch { }
            return false;
        }

        /// <summary>
        /// 取消继承指定的角色
        /// </summary>
        /// <param name="uid">当前用户ID</param>
        /// <param name="rid">被继承角色ID</param>
        public static Boolean CancelInherit(String uid, String rid)
        {
            UserBindings userBindings = UserBindings.New;
            userBindings.UserID = uid;
            userBindings.RoleID = rid;
            return userBindings.Delete() > 0;
        }
    }

}
