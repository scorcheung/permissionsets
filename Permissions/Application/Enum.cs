﻿using System;

namespace Permissions.Application
{
    /// <summary>
    /// 继承关系
    /// </summary>
    [Serializable]
    internal enum BindingRelationship
    {
        /// <summary>
        /// 无状态
        /// </summary>
        None = 0,
        /// <summary>
        /// 直接继承
        /// </summary>
        Inherit = 1,
        /// <summary>
        /// 直接被继承
        /// </summary>
        BeInherited = 2,
        /// <summary>
        /// 没有关系可继承
        /// </summary>
        CanInherit = 3,
        /// <summary>
        /// 没有关系但不可继承
        /// </summary>
        NotInherit = 4,
        /// <summary>
        /// 隔代继承
        /// </summary>
        SkipGenerationInherit = 5,
        /// <summary>
        /// 隔代被继承
        /// </summary>
        SkipGenerationBeInherit = 6
    }

    /// <summary>
    /// 权限加载类型(0-角色 1-用户)
    /// </summary>
    [Serializable]
    public enum PermissionsLoadingType
    {
        /// <summary>
        /// 角色
        /// </summary>
        Role = 0,
        /// <summary>
        /// 用户
        /// </summary>
        User = 1
    }
}
