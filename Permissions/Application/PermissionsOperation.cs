﻿using System;
using System.Collections;
using MathBasic.Collections;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using MathBasic.Managements.Data;
using Permissions.Database.Tables;

namespace Permissions.Application
{
    [Serializable]
    public sealed class PermissionsOperation
    {
        private KeyValues m_PermissionsCopied;//原始数据存储集合
        private KeyValues m_Permissions;
        private String m_ID;
        private PermissionsLoadingType m_Type;

        private const Int64 DefaultFactor = 6148914691236517205; //生成BinarySet新实例的默认Factor

        /// <summary>
        /// 生成默认的BinarySet
        /// </summary>
        public static BinarySet Default
        {
            get
            {
                return new BinarySet() { Factor = PermissionsOperation.DefaultFactor };
            }
        }

        public PermissionsOperation(String id, PermissionsLoadingType type)
            : this(id, type, true, true) { }

        public PermissionsOperation(String id, PermissionsLoadingType type, Boolean readOneself, Boolean readBings)
        {
            this.m_Permissions = new KeyValues(5);
            this.m_PermissionsCopied = new KeyValues(5);
            this.m_Type = type;
            this.m_ID = id;
            this.LoadPermissions(id, type, readOneself, readBings);
        }

        /// <summary>
        /// 加载权限数据集合
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type">权限加载类型(0-角色 1-用户)</param>
        /// <param name="readOneself">是否加载自身权限</param>
        /// <param name="readBings">是否加载继承权限</param>
        private void LoadPermissions(String id, PermissionsLoadingType type, Boolean readOneself, Boolean readBings)
        {
            if (type == PermissionsLoadingType.Role) {
                if (readOneself) {
                    foreach (RolesInfo rolesInfo in RolesInfo.Select(SqlWhereBuilder.MSSQL.Append("RoleID", id))) {
                        this.SetPermissions(rolesInfo.KeyIndex, rolesInfo.Data, rolesInfo.Filter);
                    }
                }
                if (readBings) {
                    foreach (RoleBindings roleBindings in RoleBindings.Select(SqlWhereBuilder.MSSQL.Append("RoleID", id))) {
                        this.LoadPermissions(roleBindings.ParentRoleID, type, true, true);
                    }
                }
            }
            else {
                if (readOneself) {
                    foreach (UsersInfo usersInfo in UsersInfo.Select(SqlWhereBuilder.MSSQL.Append("UserID", id))) {
                        this.SetPermissions(usersInfo.KeyIndex, usersInfo.Data, usersInfo.Filter);
                    }
                }
                if (readBings) {
                    foreach (UserBindings userBindings in UserBindings.Select(SqlWhereBuilder.MSSQL.Append("UserID", id))) {
                        this.LoadPermissions(userBindings.RoleID, PermissionsLoadingType.Role, true, true);
                    }
                }
            }
        }
        /// <summary>
        /// 添加或更新权限数据集合的成员
        /// </summary>
        /// <param name="keyIndex">主键索引</param>
        /// <param name="data">开通的权限数据</param>
        /// <param name="filter">屏蔽的权限数据</param>
        private void SetPermissions(Int32 keyIndex, Int64 data, Int64 filter)
        {
            Object key = keyIndex;
            Object value = this.m_Permissions[key];
            if (value == null) {
                BinarySet set = new BinarySet(PermissionsOperation.DefaultFactor, data, filter);
                this.m_Permissions.Add(key, set);
                this.m_PermissionsCopied.Add(key, set);
            }
            else {
                BinarySet set = (BinarySet)value;
                set.Inject(data, false);
                set.InjectFilter(filter, false);
                this.m_Permissions[key] = set;
            }
        }

        private BinarySet GetPermissionByKeyIndex(Int32 keyIndex)
        {
            Object value = this.m_Permissions[(Object)keyIndex];
            if (value == null) {
                return PermissionsOperation.Default;
            }
            else {
                return (BinarySet)value;
            }
        }

        internal void InjectByKeyIndex(Int32 keyIndex, Int32 bitLocation)
        {
            BinarySet set = this.GetPermissionByKeyIndex(keyIndex);
            set.InjectByOrdinal(bitLocation);
            this.m_Permissions[(Object)keyIndex] = set;
        }

        internal void InjectFilterByKeyIndex(Int32 keyIndex, Int32 bitLocation)
        {
            BinarySet set = this.GetPermissionByKeyIndex(keyIndex);
            set.InjectFilterByOrdinal(bitLocation);
            this.m_Permissions[(Object)keyIndex] = set;
        }

        internal void RemoveByKeyIndex(Int32 keyIndex, Int32 bitLocation)
        {
            BinarySet set = this.GetPermissionByKeyIndex(keyIndex);
            set.RejectByOrdinal(bitLocation);
            this.m_Permissions[(Object)keyIndex] = set;
        }

        internal void RemoveFilterByKeyIndex(Int32 keyIndex, Int32 bitLocation)
        {
            BinarySet set = this.GetPermissionByKeyIndex(keyIndex);
            set.RejectFilterByOrdinal(bitLocation);
            this.m_Permissions[(Object)keyIndex] = set;
        }

        internal Boolean SavePermissions()
        {
            return DbDatabase.ExecTransactions(delegate() {
                if (this.m_Type == PermissionsLoadingType.Role) {
                    foreach (DictionaryEntry dictionaryEntry in (IDictionary)this.m_Permissions) {
                        BinarySet set = (BinarySet)dictionaryEntry.Value;
                        Object key = dictionaryEntry.Key;
                        Object obj = this.m_PermissionsCopied[key];
                        if (obj != null) {
                            if (set != ((BinarySet)obj)) {
                                RolesInfo rolesInfo = RolesInfo.New;
                                rolesInfo.RoleID = this.m_ID;
                                rolesInfo.KeyIndex = (Int32)key;
                                rolesInfo.Data = set.Data;
                                rolesInfo.Filter = set.Filter;
                                if (rolesInfo.Update() <= 0) {
                                    return false;
                                }
                            }
                        }
                        else {
                            RolesInfo rolesInfo = RolesInfo.New;
                            rolesInfo.RoleID = this.m_ID;
                            rolesInfo.KeyIndex = (Int32)key;
                            rolesInfo.Data = set.Data;
                            rolesInfo.Filter = set.Filter;
                            rolesInfo.Insert();
                            this.m_PermissionsCopied.Add(key, set);
                        }
                    }
                }
                else {
                    foreach (DictionaryEntry dictionaryEntry in (IDictionary)this.m_Permissions) {
                        BinarySet set = (BinarySet)dictionaryEntry.Value;
                        Object key = dictionaryEntry.Key;
                        Object obj = this.m_PermissionsCopied[key];
                        if (obj != null) {
                            if (set != ((BinarySet)obj)) {
                                UsersInfo usersInfo = UsersInfo.New;
                                usersInfo.UserID = this.m_ID;
                                usersInfo.KeyIndex = (Int32)key;
                                usersInfo.Data = set.Data;
                                usersInfo.Filter = set.Filter;
                                if (usersInfo.Update() <= 0) {
                                    return false;
                                }
                            }
                        }
                        else {
                            UsersInfo usersInfo = UsersInfo.New;
                            usersInfo.UserID = this.m_ID;
                            usersInfo.KeyIndex = (Int32)key;
                            usersInfo.Data = set.Data;
                            usersInfo.Filter = set.Filter;
                            usersInfo.Insert();
                            this.m_PermissionsCopied.Add(key, set);
                        }
                    }
                }
                return true;
            });
        }

        public Boolean VerifyPermissionByKeyIndex(Int32 keyIndex, out BinarySet permission)
        {
            Object value = this.m_Permissions[(Object)keyIndex];
            if (value == null) {
                permission = PermissionsOperation.Default;
                return false;
            }
            else {
                permission = (BinarySet)value;
                return true;
            }
        }

        public Boolean Verify(Int32 keyIndex, Int32 bitLocation)
        {
            Object value = this.m_Permissions[(Object)keyIndex];
            if (value != null) {
                BinarySet set = (BinarySet)value;
                return set.VerifyByOrdinal(bitLocation);
            }
            else {
                return false;
            }
        }
    }
}
