using System;
using System.Data;
using System.Collections.Generic;
using MathBasic;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using MathBasic.Managements;
using Permissions.Database;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model;
using Permissions.Database.Table.Model.Interfaces;
using Permissions.Database.View.Model;
using Permissions.Database.View.Model.Interfaces;

namespace Permissions.Database.Tables
{
    internal sealed partial class RoleBindings : TableScheme, IRoleBindings
	{
		#region 实例自身相关的操作

        static RoleBindings()
        {
            RoleBindings scheme = RoleBindings.New;
            RoleBindings.m_ClassTableName = scheme.TableName;
            RoleBindings.m_PrimaryKeyName = Helpers.GetSpecificPrimaryKey(scheme.GetPrimaryFields());
        }

        private RoleBindings()
        {
            base.DbConnectionSelection = Helpers.DatabaseConnectionSelection;
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static RoleBindings New
        {
            get
            {
                return new RoleBindings();
            }
        }

		#endregion

		#region 获得表映射类的相关信息

		private static String m_PrimaryKeyName;
		/// <summary>
		/// 获取此类所映射的表的某个主键名称。
		/// </summary>
		public static String GetPrimaryKeyName()
		{
			return RoleBindings.m_PrimaryKeyName;
		}

		private static String m_ClassTableName;
		/// <summary>
		/// 获取此类所映射的表的名称。
		/// </summary>
		public static String GetClassTableName()
		{
			return RoleBindings.m_ClassTableName;
		}

		#endregion

		#region 已映射到表的字段属性

		/// <summary>
		/// 
		/// </summary>
        [DbFieldDataInfo(FieldTypes.Primary | FieldTypes.CanInsert)]
		public String RoleID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
        [DbFieldDataInfo(FieldTypes.Primary | FieldTypes.CanInsert)]
		public String ParentRoleID
		{ get; set; }


		#endregion

        #region 数据库查询及操作方法

        /// <summary>
        /// 查询是否有符合条件的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Boolean Exists(SqlWhereBuilder where)
        {
            return TableBaseAdapter.Exists<RoleBindings>(where);
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount(SqlWhereBuilder where)
        {
            return Helpers.GetDataCount<RoleBindings>(RoleBindings.GetPrimaryKeyName(), where);
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static RoleBindings SelectSingleOnly(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.SelectSingleOnly<RoleBindings>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetData<RoleBindings>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetTop<RoleBindings>(top, where, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<RoleBindings> GetDataByTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetDataByTop<RoleBindings>(top, where, fields);
        }

        /// <summary>
        /// 获得数据列表。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<RoleBindings> Select(SqlWhereBuilder where, params string[] fields)
        {
            return TableBaseAdapter.Select<RoleBindings>(where, true, fields, false, false, true);
        }

        /// <summary>
        /// 获得所有数据列表。
        /// </summary>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<RoleBindings> SelectAll(params string[] fields)
        {
            return RoleBindings.Select(SqlWhereBuilder.None, fields);
        }

        /// <summary>
        /// 向指定的数据库表中插入数据。
        /// </summary>
        /// <param name="fieldsWithValue">要插入的列与值。</param>
        public static Object InsertBy(KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.InsertBy<RoleBindings>(fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中更新匹配数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fieldsWithValue">要更新的列与值。</param>
        public static Int32 UpdateBy(SqlWhereBuilder where, KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.UpdateBy<RoleBindings>(where, fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中删除匹配数据。
        /// </summary>
        /// <param name="where">查询条件</param>
        public static Int32 DeleteBy(SqlWhereBuilder where)
        {
            return TableBaseAdapter.DeleteBy<RoleBindings>(where);
        }

        #endregion
	}
}
