using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.View.Model.Interfaces
{
    internal partial interface IV_RolesByUserBindingsRID : IUserBindings, IRoles
	{
		/// <summary>
		/// 获得映射出来的表 UserBindings 的实例数据部分。
		/// </summary>
		UserBindings Get_UserBindings_Part();

		/// <summary>
		/// 获得映射出来的表 Roles 的实例数据部分。
		/// </summary>
		Roles Get_Roles_Part();

	}
}