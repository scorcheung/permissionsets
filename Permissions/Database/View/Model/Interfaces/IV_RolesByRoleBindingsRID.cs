using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.View.Model.Interfaces
{
    internal partial interface IV_RolesByRoleBindingsRID : IRoleBindings, IRoles
	{
		/// <summary>
		/// 获得映射出来的表 RoleBindings 的实例数据部分。
		/// </summary>
		RoleBindings Get_RoleBindings_Part();

		/// <summary>
		/// 获得映射出来的表 Roles 的实例数据部分。
		/// </summary>
		Roles Get_Roles_Part();

	}
}