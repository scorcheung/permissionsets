﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.View.Model;

namespace Permissions.Database.Views
{
    /// <summary>
    /// 存储过程SP_GetAllChildRoles的返回结果类
    /// </summary>
    internal sealed class S_RolesInfo : ViewScheme
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        [DbFieldDataInfo(FieldTypes.Simple)]
        public string RoleID
        {
            get; set;
        }
        /// <summary>
        /// 角色名称
        /// </summary>
        [DbFieldDataInfo(FieldTypes.Simple)]
        public string RoleName
        {
            get; set;
        }
    }
}
