using System;
using System.Data;
using System.Collections.Generic;
using MathBasic;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using MathBasic.Managements;
using Permissions.Database;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model;
using Permissions.Database.Table.Model.Interfaces;
using Permissions.Database.View.Model;
using Permissions.Database.View.Model.Interfaces;

namespace Permissions.Database.Views
{
    internal sealed partial class V_RolesByRoleBindingsPID : ViewScheme, IV_RolesByRoleBindingsPID
	{
		#region 实例自身相关的操作

        static V_RolesByRoleBindingsPID()
        {
            V_RolesByRoleBindingsPID scheme = V_RolesByRoleBindingsPID.New;
            V_RolesByRoleBindingsPID.m_ClassTableName = scheme.TableName;
            V_RolesByRoleBindingsPID.m_PrimaryKeyName = Helpers.GetSpecificPrimaryKey(scheme.GetPrimaryFields());
        }

        private V_RolesByRoleBindingsPID()
        {
            base.DbConnectionSelection = Helpers.DatabaseConnectionSelection;
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static V_RolesByRoleBindingsPID New
        {
            get
            {
                return new V_RolesByRoleBindingsPID();
            }
        }

		#endregion

		#region 获得视图映射类的相关信息

		private static String m_PrimaryKeyName;
		/// <summary>
		/// 获取此类所映射的视图的某个主键名称。
		/// </summary>
		public static String GetPrimaryKeyName()
		{
			return V_RolesByRoleBindingsPID.m_PrimaryKeyName;
		}

		private static String m_ClassTableName;
		/// <summary>
		/// 获取此类所映射的视图的名称。
		/// </summary>
		public static String GetClassTableName()
		{
			return V_RolesByRoleBindingsPID.m_ClassTableName;
		}

		#endregion

		#region 获得视图映射出来的各表的映射实例的方法

		/// <summary>
		/// 获得映射出来的表 RoleBindings 的实例数据部分。
		/// </summary>
		public RoleBindings Get_RoleBindings_Part()
		{
			RoleBindings data = RoleBindings.New;
			Helpers.CopyViewFields(this, data);
			return data;
		}

		/// <summary>
		/// 获得映射出来的表 Roles 的实例数据部分。
		/// </summary>
		public Roles Get_Roles_Part()
		{
			Roles data = Roles.New;
			Helpers.CopyViewFields(this, data);
			return data;
		}

		#endregion

		#region 已映射到视图的各表的字段属性

		/// <summary>
		/// 表 Roles 中的字段解释为：   
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String RoleName
		{ get; set; }


		/// <summary>
		/// 表 Roles 中的字段解释为：   
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String Description
		{ get; set; }


		/// <summary>
		/// 表 RoleBindings 中的字段解释为：   
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String RoleID
		{ get; set; }


		/// <summary>
		/// 表 RoleBindings 中的字段解释为：   
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String ParentRoleID
		{ get; set; }


		#endregion

		#region 没有映射到视图的各表的字段属性

		/// <summary>
		/// 
		/// </summary>
		[NotIncluding]
		String IRoles.ID
		{ get { throw new NotImplementedException(); } set { throw new NotImplementedException(); } }


		#endregion

        #region 数据库查询及操作方法

        /// <summary>
        /// 查询是否有符合条件的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Boolean Exists(SqlWhereBuilder where)
        {
            return TableBaseAdapter.Exists<V_RolesByRoleBindingsPID>(where);
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount(SqlWhereBuilder where)
        {
            return Helpers.GetDataCount<V_RolesByRoleBindingsPID>(V_RolesByRoleBindingsPID.GetPrimaryKeyName(), where);
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static V_RolesByRoleBindingsPID SelectSingleOnly(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.SelectSingleOnly<V_RolesByRoleBindingsPID>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetData<V_RolesByRoleBindingsPID>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetTop<V_RolesByRoleBindingsPID>(top, where, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<V_RolesByRoleBindingsPID> GetDataByTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetDataByTop<V_RolesByRoleBindingsPID>(top, where, fields);
        }

        /// <summary>
        /// 获得数据列表。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<V_RolesByRoleBindingsPID> Select(SqlWhereBuilder where, params string[] fields)
        {
            return TableBaseAdapter.Select<V_RolesByRoleBindingsPID>(where, true, fields, false, false, true);
        }

        /// <summary>
        /// 获得所有数据列表。
        /// </summary>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<V_RolesByRoleBindingsPID> SelectAll(params string[] fields)
        {
            return V_RolesByRoleBindingsPID.Select(SqlWhereBuilder.None, fields);
        }

        #endregion
	}
}
