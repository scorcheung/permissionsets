using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.Table.Model.Interfaces
{
    internal partial interface IUserBindings
	{
		/// <summary>
		/// 
		/// </summary>
		String UserID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String RoleID
		{ get; set; }


	}
}