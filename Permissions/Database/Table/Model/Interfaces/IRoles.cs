using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.Table.Model.Interfaces
{
    internal partial interface IRoles
	{

		/// <summary>
		/// 
		/// </summary>
		String ID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String RoleName
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String Description
		{ get; set; }


	}
}