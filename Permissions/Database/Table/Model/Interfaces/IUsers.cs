using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.Table.Model.Interfaces
{
    internal partial interface IUsers
	{
		/// <summary>
		/// 
		/// </summary>
		String ID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String UserName
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String Password
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Boolean Super
		{ get; set; }


	}
}