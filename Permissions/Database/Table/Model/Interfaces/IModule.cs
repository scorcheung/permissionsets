using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.Table.Model.Interfaces
{
    internal partial interface IModule
	{
		/// <summary>
		/// 
		/// </summary>
		Int32 ModuleID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		String ModuleName
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int32 ParentID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int32 KeyIndex
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int32 BitLocation
		{ get; set; }


	}
}