using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using Permissions.Database.Tables;
using Permissions.Database.Table.Model.Interfaces;

namespace Permissions.Database.Table.Model.Interfaces
{
    internal partial interface IRolesInfo
	{
		/// <summary>
		/// 
		/// </summary>
		String RoleID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int32 KeyIndex
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int64 Data
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int64 Filter
		{ get; set; }


	}
}