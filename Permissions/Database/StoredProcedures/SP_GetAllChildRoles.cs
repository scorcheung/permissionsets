using System;
using System.Collections.Generic;
using System.Data;
using MathBasic.ExtendedModules.Ext1.Database.StoredProcedure.Model;
using MathBasic.Managements;
using Permissions.Database.StoredProcedure.Model;
using Permissions.Database.Table.Model;

namespace Permissions.Database.StoredProcedures
{
    internal sealed partial class SP_GetAllChildRoles : StoredProcedureScheme
    {
        #region 实例自身相关的操作

        static SP_GetAllChildRoles()
        {
            SP_GetAllChildRoles scheme = SP_GetAllChildRoles.New;
            SP_GetAllChildRoles.m_ClassStoredProcedureName = scheme.StoredProcedureName;
        }

        private SP_GetAllChildRoles()
        {
            base.DbConnectionSelection = Helpers.DatabaseConnectionSelection;
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static SP_GetAllChildRoles New
        {
            get
            {
                return new SP_GetAllChildRoles();
            }
        }

        #endregion

		#region 获得表映射类的相关信息

		private static String m_ClassStoredProcedureName;
		/// <summary>
		/// 获取此类所映射的存储过程的名称。
		/// </summary>
		public static String GetClassStoredProcedureName()
		{
			return SP_GetAllChildRoles.m_ClassStoredProcedureName;
		}

		#endregion

        #region 存储过程的相关参数

        /// <summary>
        /// 
        /// </summary>
        [DbParameterDataInfo(ParameterDirection.Input, 32)]
        public String ParentRoleID { get; set; }

        /// <summary>
        /// 获取存储过程的返回值。
        /// </summary>
        [DbParameterDataInfo(ParameterDirection.ReturnValue)]
        public Int32 ReturnedValue { get; private set; }

        /// <summary>
        /// 获取存储过程查询产生的数据集合。
        /// </summary>
        public IEnumerable<T> Select<T>() where T : TableSchemeBase
        {
            return base.SelectByExecution<T>(true);
        }

        #endregion

    }

}
