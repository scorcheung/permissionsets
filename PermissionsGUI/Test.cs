﻿using System.Windows.Forms;
using Permissions.Application;

namespace PermissionsGUI
{
    public partial class Test : Form
    {
        public Test()
        {
            InitializeComponent();
        }

        private void Test_Load(object sender, System.EventArgs e)
        {
            OperationAdapter.DatabaseConnectionSelection = "server=server.workgroup,65000;database=Permissions;uid=sa;pwd=sa";
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            OperationAdapter.ShowDialogRolesForm(this);
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            OperationAdapter.ShowDialogUsersForm(this);
        }
    }
}
