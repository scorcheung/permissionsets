﻿using System;
using System.Windows.Forms;
using Permissions.Application;

namespace PermissionsGUI
{
    public partial class Default : Form
    {
        public Default()
        {
            this.InitializeComponent();
        }

        private void Default_Load(object sender, EventArgs e)
        {
            OperationAdapter.DatabaseConnectionSelection = "Data Source=(LocalDB)\\ProjectsV13;Database=Permissions;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;MultipleActiveResultSets=True";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OperationAdapter.ShowDialogRolesForm(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OperationAdapter.ShowDialogUsersForm(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string keyindexStr = this.textBox1.Text;
            string locationStr = this.textBox2.Text;
            string name = this.textBox3.Text;
            if (keyindexStr.Length > 0 && locationStr.Length > 0 && name.Length > 0)
            {
                Int32 keyindex;
                Int32 location;
                if (Int32.TryParse(keyindexStr, out keyindex) && Int32.TryParse(locationStr, out location))
                {
                    if (location > 63)
                    {
                        this.lbResult.Text = "位的值不能大于63。";
                    }
                    else
                    {
                        PermissionsOperation permissionsOperation = new PermissionsOperation(name, this.radioButton1.Checked ? PermissionsLoadingType.Role : PermissionsLoadingType.User);
                        this.lbResult.Text = permissionsOperation.Verify(keyindex, location) ? "有权限" : "无权限";
                    }
                }
                else
                {
                    this.lbResult.Text = "输入的项必须是数字格式。";
                }
            }
            else
            {
                this.lbResult.Text = "不能有空值。";
            }
        }
    }
}
