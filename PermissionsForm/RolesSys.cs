﻿using System;
using System.Windows.Forms;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Application;
using Permissions.Database.Tables;
using Permissions.Helper;
using Permissions.Tools;

namespace Permissions
{
    internal sealed partial class RolesSys : Form
    {
        private RolesSys()
        {
            this.InitializeComponent();
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow)
        {
            return new RolesSys().ShowDialog(parentWindow);
        }

        private void RolesSys_Load(object sender, EventArgs e)
        {
            this.BindRoles();
        }

        private void tsmiInherit_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = this.dgvRoleList.SelectedRows[0].Cells;
            SetInherit.ShowDialogBy(this, (String)cells[0].Value, (String)cells[1].Value, PermissionsLoadingType.Role);
        }

        private void tsmiSet_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = this.dgvRoleList.SelectedRows[0].Cells;
            SetPermissions.ShowDialogBy(this, (String)cells[0].Value, (String)cells[1].Value, PermissionsLoadingType.Role);
        }

        private void tsmiAdd_Click(object sender, EventArgs e)
        {
            if (RolesEdit.ShowDialogBy(this) == DialogResult.OK)
            {
                this.BindRoles();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.BindRoles();
        }

        private void BindRoles()
        {
            SqlWhereBuilder where = null;
            String search = this.txtSearch.Text.Trim();
            if (search.Length > 0)
            {
                where = SqlWhereBuilder.MSSQL.AppendGeneralText("RoleName like '%" + DevTools.SafeTrimSql(search) + "%'");
            }
            else
            {
                where = SqlWhereBuilder.None;
            }
            DataGridViewRowCollection rows = this.dgvRoleList.Rows;
            rows.Clear();
            foreach (Roles roles in Roles.Select(where))
            {
                DataGridViewCellCollection cells = rows[rows.Add(roles.ID, roles.RoleName)].Cells;
                cells[1].ToolTipText = roles.Description;
            }
        }

        private void tsmiDelete_Click(object sender, EventArgs e)
        {
            if (SysMessages.Show(this, "确定删除角色吗？", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Object id = this.dgvRoleList.SelectedRows[0].Cells[0].Value;
                if (RoleBindings.Exists(SqlWhereBuilder.MSSQL.Append("ParentRoleID", id)) ||
                    UserBindings.Exists(SqlWhereBuilder.MSSQL.Append("RoleID", id)))
                {
                    SysMessages.Show(this, "当前角色已被继承不能删除。", MessageBoxButtons.OK);
                }
                else
                {
                    if (DbDatabase.ExecTransactions(delegate()
                    {
                        try
                        {
                            RoleBindings.DeleteBy(SqlWhereBuilder.MSSQL.Append("RoleID", id));
                            RolesInfo.DeleteBy(SqlWhereBuilder.MSSQL.Append("RoleID", id));
                            Roles roles = Roles.New;
                            roles.ID = (String)id;
                            roles.Delete();
                            return true;
                        }
                        catch { }
                        return false;
                    }))
                    {
                        this.BindRoles();
                    }
                    else
                    {
                        SysMessages.Show(this, InformationTip.Fail);
                    }
                }
            }
        }

        private void tsmiEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rows = this.dgvRoleList.SelectedRows;
            if (rows.Count > 0)
            {
                if (RolesEdit.ShowDialogBy(this, (String)rows[0].Cells[0].Value) == DialogResult.OK)
                {
                    this.BindRoles();
                }
            }
            else
            {
                SysMessages.Show(this, "请选择要修改的角色。", MessageBoxButtons.OK);
            }
        }

        private void dgvRoleList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Int32 index = e.RowIndex;
                if (index >= 0)
                {
                    this.dgvRoleList.Rows[index].Selected = true;
                    this.contextMenuStrip1.Show(Form.MousePosition);
                }
            }
        }

    }
}
