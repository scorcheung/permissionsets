﻿using System;
using System.Windows.Forms;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Application;
using Permissions.Database.Tables;
using Permissions.Helper;
using Permissions.Tools;

namespace Permissions
{
    internal sealed partial class UsersSys : Form
    {
        private UsersSys()
        {
            this.InitializeComponent();
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow)
        {
            return new UsersSys().ShowDialog(parentWindow);
        }

        private void RolesSys_Load(object sender, EventArgs e)
        {
            this.Bind();
        }

        private void tsmiInherit_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = this.dgvUsersList.SelectedRows[0].Cells;
            SetInherit.ShowDialogBy(this, (String)cells[0].Value, (String)cells[2].Value, PermissionsLoadingType.User);
        }

        private void tsmiSet_Click(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = this.dgvUsersList.SelectedRows[0].Cells;
            SetPermissions.ShowDialogBy(this, (String)cells[0].Value, (String)cells[2].Value, PermissionsLoadingType.User);
        }

        private void dgvUsersList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Int32 index = e.RowIndex;
                if (index >= 0)
                {
                    DataGridViewRow row = this.dgvUsersList.Rows[index];
                    if (!(Boolean)row.Cells[1].Value)
                    {
                        row.Selected = true;
                        this.contextMenuStrip1.Show(Form.MousePosition);
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Bind();
        }

        private void tsmiClear_Click(object sender, EventArgs e)
        {
            if (SysMessages.Show(this, "确定清空所有权限吗？", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                if (!OperationAdapter.ClearUserPermissions((String)this.dgvUsersList.SelectedRows[0].Cells[0].Value))
                {
                    SysMessages.Show(this, InformationTip.Fail);
                }
            }
        }

        private void Bind()
        {
            SqlWhereBuilder where = null;
            String search = this.txtSearch.Text.Trim();
            if (search.Length > 0)
            {
                where = SqlWhereBuilder.MSSQL.AppendGeneralText("UserName like '%" + DevTools.SafeTrimSql(search) + "%'");
            }
            else
            {
                where = SqlWhereBuilder.MSSQL.AppendGeneralText("1=1");
            }
            where.AppendGeneralText(" ORDER BY Super DESC");
            DataGridViewRowCollection rows = this.dgvUsersList.Rows;
            rows.Clear();
            foreach (Users roles in Users.Select(where))
            {
                Boolean super = roles.Super;
                rows.Add(roles.ID, super, roles.UserName, super ? "系统用户" : "一般用户");
            }
        }

    }

}
