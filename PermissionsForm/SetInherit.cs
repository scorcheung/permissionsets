﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Application;
using Permissions.Database.Tables;
using Permissions.Database.Views;
using Permissions.Helper;

namespace Permissions
{
    internal sealed partial class SetInherit : Form
    {

        private String m_Id;
        private String m_Name;
        private PermissionsLoadingType m_PermissionsLoadingType;
        private Boolean m_Refresh;
        private Boolean[] m_LoadNodes;
        private BindingRelationship[] m_BindRelationing;

        private SetInherit()
        {
            this.InitializeComponent();
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow, String id, String name, PermissionsLoadingType permissionsLoadingType)
        {
            return new SetInherit()
            {
                m_Id = id,
                m_Name = name,
                m_PermissionsLoadingType = permissionsLoadingType
            }.ShowDialog(parentWindow);
        }

        private void SetRolesInherit_Load(object sender, EventArgs e)
        {
            this.Text = String.Format("{1} - {0}继承角色", (this.m_PermissionsLoadingType == PermissionsLoadingType.Role ? "角色" : "用户"), m_Name);
            this.Bind();
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            this.BindNode(e.Node);
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeNode node = e.Node;
                this.treeView1.SelectedNode = node;
                if (node.Level == 0)
                {
                    String tag = (String)node.Tag;
                    if (this.m_PermissionsLoadingType == PermissionsLoadingType.Role)
                    {
                        if (this.m_Id.Equals(tag, StringComparison.Ordinal))
                        {
                            return;
                        }
                    }
                    BindingRelationship type = this.m_BindRelationing[node.Index];
                    if (type == BindingRelationship.None)
                    {
                        if (this.m_PermissionsLoadingType == PermissionsLoadingType.Role)
                        {
                            type = this.m_BindRelationing[node.Index] = RolesOperation.CheckUpWhetherCanInherit(this.m_Id, tag);
                        }
                        else
                        {
                            type = this.m_BindRelationing[node.Index] = UsersOperation.CheckUpWhetherCanInherit(this.m_Id, tag);
                        }
                    }
                    switch (type)
                    {
                        case BindingRelationship.Inherit:
                            this.tsmiBind.Visible = false;
                            this.tsmiCannel.Visible = true;
                            this.tsmiCannel.Tag = 0;
                            this.contextMenuStrip1.Show(Form.MousePosition);
                            break;
                        case BindingRelationship.SkipGenerationInherit:
                        case BindingRelationship.CanInherit:
                            this.tsmiBind.Visible = true;
                            this.tsmiCannel.Visible = false;
                            this.tsmiBind.Tag = 0;
                            this.contextMenuStrip1.Show(Form.MousePosition);
                            break;
                    }
                }
            }
        }

        private void tsmiBind_Click(object sender, EventArgs e)
        {
            if ((Int32)this.tsmiBind.Tag == 0)
            {
                TreeNode node = this.treeView1.SelectedNode;
                Boolean isRole = this.m_PermissionsLoadingType == PermissionsLoadingType.Role;
                if ((isRole && RolesOperation.Inherit(this.m_Id, (String)node.Tag)) ||
                    (!isRole && UsersOperation.Inherit(this.m_Id, (String)node.Tag)))
                {
                    node.Text += "(√)";
                    this.m_BindRelationing[node.Index] = BindingRelationship.Inherit;
                    this.m_LoadNodes[node.Index] = false;
                    this.BindNode(node);
                    this.BindNode(this.m_Id);
                    this.m_Refresh = true;
                }
                else
                {
                    SysMessages.Show(this, InformationTip.Fail);
                }
            }
        }

        private void tsmiCannel_Click(object sender, EventArgs e)
        {
            if ((Int32)this.tsmiCannel.Tag == 0)
            {
                TreeNode node = this.treeView1.SelectedNode;
                Boolean isRole = this.m_PermissionsLoadingType == PermissionsLoadingType.Role;
                if ((isRole && RolesOperation.CancelInherit(this.m_Id, (String)node.Tag)) ||
                     (!isRole && UsersOperation.CancelInherit(this.m_Id, (String)node.Tag)))
                {
                    String text = node.Text;
                    Int32 index = text.IndexOf('(');
                    if (index > 0)
                    {
                        node.Text = text.Substring(0, index);
                    }
                    this.m_BindRelationing[node.Index] = BindingRelationship.CanInherit;
                    this.m_LoadNodes[node.Index] = false;
                    this.BindNode(node);
                    this.BindNode(this.m_Id);
                    this.m_Refresh = true;
                }
                else
                {
                    SysMessages.Show(this, InformationTip.Fail);
                }
            }
        }

        /// <summary>
        /// 将继承关系绑定到树节点
        /// </summary>
        /// <param name="roleId"></param>
        private void BindNode(String roleId)
        {
            foreach (TreeNode node in this.treeView1.Nodes)
            {
                if (node.Tag != null && (String)node.Tag == roleId)
                {
                    this.m_LoadNodes[node.Index] = false;
                    BindNode(node);
                    return;
                }
            }
        }

        /// <summary>
        /// 将继承关系绑定到树节点
        /// </summary>
        /// <param name="node"></param>
        private void BindNode(TreeNode node)
        {
            if (node.Level == 0)
            {
                if (!this.m_LoadNodes[node.Index])
                {
                    this.m_LoadNodes[node.Index] = true;
                    String id = (String)node.Tag;
                    TreeNodeCollection nodes = node.Nodes;
                    TreeNodeCollection treeNodeCollection = nodes[0].Nodes;
                    treeNodeCollection.Clear();
                    Boolean isRole = this.m_PermissionsLoadingType == PermissionsLoadingType.Role;
                    foreach (V_RolesByRoleBindingsPID v_RolesByRoleBindingsPID in V_RolesByRoleBindingsPID.Select(SqlWhereBuilder.MSSQL.Append("RoleID", id)))
                    {
                        TreeNode nodeLast = treeNodeCollection.Add(v_RolesByRoleBindingsPID.RoleName);
                        nodeLast.ToolTipText = v_RolesByRoleBindingsPID.Description;
                        nodeLast.Tag = v_RolesByRoleBindingsPID.ParentRoleID;
                        if (isRole && this.m_Id.Equals(v_RolesByRoleBindingsPID.ParentRoleID, StringComparison.Ordinal))
                        {
                            nodeLast.ForeColor = Color.Gray;
                        }
                    }
                    if (treeNodeCollection.Count <= 0)
                    {
                        treeNodeCollection.Add("(无)");
                    }

                    treeNodeCollection = nodes[1].Nodes;
                    treeNodeCollection.Clear();
                    foreach (V_RolesByRoleBindingsRID v_RolesByRoleBindingsRID in V_RolesByRoleBindingsRID.Select(SqlWhereBuilder.MSSQL.Append("ParentRoleID", id)))
                    {
                        TreeNode nodeLast = treeNodeCollection.Add(v_RolesByRoleBindingsRID.RoleName);
                        nodeLast.ToolTipText = v_RolesByRoleBindingsRID.Description;
                        nodeLast.Tag = v_RolesByRoleBindingsRID.RoleID;
                        if (isRole && this.m_Id.Equals(v_RolesByRoleBindingsRID.RoleID, StringComparison.Ordinal))
                        {
                            nodeLast.ForeColor = Color.Gray;
                        }
                    }
                    if (treeNodeCollection.Count <= 0)
                    {
                        treeNodeCollection.Add("(无)");
                    }
                }
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            if (node.Level == 2)
            {
                String tag = (String)node.Tag;
                TreeNodeCollection nodes = this.treeView1.Nodes;
                Int32 count = nodes.Count;
                while (--count >= 0)
                {
                    TreeNode currentNode = nodes[count];
                    if (((String)currentNode.Tag).Equals(tag, StringComparison.Ordinal))
                    {
                        this.treeView1.SelectedNode = currentNode;
                    }
                }
            }
        }

        private void tsmiSet_Click(object sender, EventArgs e)
        {
            TreeNode node = this.treeView1.SelectedNode;
            String name = node.Text;
            Int32 index = name.LastIndexOf('(');
            if (index >= 0)
            {
                name = name.Substring(0, name.LastIndexOf('('));
            }
            if (SetPermissions.ShowDialogBy(this, (String)node.Tag, name, PermissionsLoadingType.Role) == DialogResult.Yes)
            {
                this.m_Refresh = true;
                this.Bind();
            }
        }

        private void SetInherit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_Refresh)
            {
                this.DialogResult = DialogResult.Yes;
            }
        }

        private void Bind()
        {
            Boolean isRole = this.m_PermissionsLoadingType == PermissionsLoadingType.Role;
            List<String> inheritIdList = new List<String>();
            if (isRole)
            {
                foreach (V_RolesByRoleBindingsPID v_RolesByRoleBindingsPID in V_RolesByRoleBindingsPID.Select(SqlWhereBuilder.MSSQL.Append("RoleID", this.m_Id)))
                {
                    inheritIdList.Add(v_RolesByRoleBindingsPID.ParentRoleID);
                }
            }
            else
            {
                foreach (V_RolesByUserBindingsRID v_RolesByUserBindingsRID in V_RolesByUserBindingsRID.Select(SqlWhereBuilder.MSSQL.Append("UserID", this.m_Id)))
                {
                    inheritIdList.Add(v_RolesByUserBindingsRID.RoleID);
                }
            }

            Int32 count = 0;
            //--绑定角色列表
            TreeNodeCollection nodes = this.treeView1.Nodes;
            nodes.Clear();
            var childRoles = RolesOperation.GetAllChildRolesPro(this.m_Id);
            var parentRoles = RolesOperation.GetAllParentRolesPro(this.m_Id);
            foreach (Roles roles in Roles.Select(SqlWhereBuilder.None))
            {
                String name = roles.RoleName;
                Int32 length = inheritIdList.Count;
                while (--length >= 0)
                {
                    if (inheritIdList[length].Equals(roles.ID, StringComparison.Ordinal))
                    {
                        name += "(√)";
                        break;
                    }
                }
                TreeNode node = nodes.Add(name);
                node.ToolTipText = roles.Description;
                node.Tag = roles.ID;
                // 判断是否为当前操作的角色
                if (isRole && this.m_Id.Equals(roles.ID, StringComparison.Ordinal))
                {
                    node.ForeColor = Color.Gray;
                }
                if (isRole)
                {
                    //判断是否为子孙角色节点
                    foreach (var item in childRoles)
                    {
                        if (item.RoleID == roles.ID)
                        {
                            node.ForeColor = Color.Gray;
                        }
                    }
                }
                TreeNodeCollection treeNodeCollection = node.Nodes;
                treeNodeCollection.Add("隶属于");
                treeNodeCollection.Add("指派到");
                count++;
            }

            this.m_LoadNodes = new Boolean[count];
            this.m_BindRelationing = new BindingRelationship[count];
        }
    }
}
