﻿using System.Windows.Forms;

namespace Permissions.Helper
{
    internal sealed class SysMessages
    {
        /// <summary>
        /// 显示消息框
        /// </summary>
        /// <param name="form">要显示消息框的窗体</param>
        /// <param name="infortip">成功或失败的枚举值</param>
        /// <returns></returns>
        public static DialogResult Show(IWin32Window form, InformationTip infortip)
        {
            DialogResult dr = DialogResult.None;
            switch (infortip)
            {
                case InformationTip.Success:
                    dr = MessageBox.Show(form, "操作成功！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case InformationTip.Fail:
                    dr = MessageBox.Show(form, "操作失败！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            return dr;
        }

        /// <summary>
        /// 显示消息框
        /// </summary>
        /// <param name="form">要显示消息框的窗体</param>
        /// <param name="InfoTipText">消息框中显示的信息</param>
        /// <param name="messageBoxButtons">消息框中的按钮的类型</param>
        /// <returns></returns>
        public static DialogResult Show(IWin32Window form, string InfoTipText, MessageBoxButtons messageBoxButtons)
        {
            return MessageBox.Show(form, InfoTipText, "系统提示", messageBoxButtons, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示错误消息框
        /// </summary>
        /// <param name="form">要显示消息框的窗体</param>
        public static void Error(IWin32Window form)
        {
            MessageBox.Show(form, "系统异常！", "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }


    public enum InformationTip
    {
        // 摘要:
        //     成功提示
        Success = 0,
        //
        // 摘要:
        //     失败提示
        Fail = 1,
    }
}
