﻿namespace Permissions
{
    partial class SetInherit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetInherit));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiBind = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCannel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSet = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.ItemHeight = 18;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(345, 411);
            this.treeView1.TabIndex = 0;
            this.treeView1.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBind,
            this.tsmiCannel,
            this.toolStripSeparator1,
            this.tsmiSet});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 76);
            // 
            // tsmiBind
            // 
            this.tsmiBind.Image = global::Permissions.Properties.Resources.bangdin;
            this.tsmiBind.Name = "tsmiBind";
            this.tsmiBind.Size = new System.Drawing.Size(124, 22);
            this.tsmiBind.Text = "绑定(&C)";
            this.tsmiBind.Click += new System.EventHandler(this.tsmiBind_Click);
            // 
            // tsmiCannel
            // 
            this.tsmiCannel.Image = global::Permissions.Properties.Resources.jiebang;
            this.tsmiCannel.Name = "tsmiCannel";
            this.tsmiCannel.Size = new System.Drawing.Size(124, 22);
            this.tsmiCannel.Text = "解绑(&R)";
            this.tsmiCannel.Click += new System.EventHandler(this.tsmiCannel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // tsmiSet
            // 
            this.tsmiSet.Image = global::Permissions.Properties.Resources.set;
            this.tsmiSet.Name = "tsmiSet";
            this.tsmiSet.Size = new System.Drawing.Size(124, 22);
            this.tsmiSet.Text = "权限设定";
            this.tsmiSet.Click += new System.EventHandler(this.tsmiSet_Click);
            // 
            // SetInherit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 411);
            this.Controls.Add(this.treeView1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(361, 449);
            this.Name = "SetInherit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "角色继承";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetInherit_FormClosing);
            this.Load += new System.EventHandler(this.SetRolesInherit_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiBind;
        private System.Windows.Forms.ToolStripMenuItem tsmiCannel;
        private System.Windows.Forms.ToolStripMenuItem tsmiSet;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;


    }
}