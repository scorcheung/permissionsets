﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MathBasic;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using MathBasic.Managements.Data;
using Permissions.Application;
using Permissions.Database;
using Permissions.Database.Tables;
using Permissions.Helper;

namespace Permissions
{
    internal sealed partial class SetPermissions : Form
    {
        private PermissionsOperation m_OneselfPermissionsOperation;
        private PermissionsOperation m_BindingsPermissionsOperation;
        private PermissionsLoadingType m_PermissionsLoadingType;
        private Boolean m_Refresh;
        private String m_Id;
        private String m_Name;

        private SetPermissions()
        {
            this.InitializeComponent();
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow, String id, String name, PermissionsLoadingType permissionsLoadingType)
        {
            return new SetPermissions()
            {
                m_PermissionsLoadingType = permissionsLoadingType,
                m_Id = id,
                m_Name = name
            }.ShowDialog(parentWindow);
        }

        private void SetRolesPermissions_Load(object sender, EventArgs e)
        {
            this.Text = String.Format("{1} - {0}权限设定", (this.m_PermissionsLoadingType == PermissionsLoadingType.Role ? "角色" : "用户"), m_Name);
            this.Bind();
        }

        private void SetRolesPermissions_Resize(object sender, EventArgs e)
        {
            this.groupBox1.Size =
                this.groupBox2.Size = new Size(this.panel1.Width / 2 - 15, this.groupBox1.Height);
            Point point = this.groupBox2.Location;
            point.X = this.groupBox1.Width + 20;
            this.groupBox2.Location = point;
        }

        private void llOpen1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (this.llOpen1.Text == "全部展开")
            {
                this.llOpen1.Text = "全部折叠";
                this.tvData.ExpandAll();
                if (this.tvData.SelectedNode == null)
                {
                    this.tvData.SelectedNode = this.tvData.Nodes[0];
                }
            }
            else
            {
                this.llOpen1.Text = "全部展开";
                this.tvData.CollapseAll();
            }
        }

        private void llOpen2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (this.llOpen2.Text == "全部展开")
            {
                this.llOpen2.Text = "全部折叠";
                this.tvFilter.ExpandAll();
                if (this.tvData.SelectedNode == null)
                {
                    this.tvFilter.SelectedNode = this.tvFilter.Nodes[0];
                }
            }
            else
            {
                this.llOpen2.Text = "全部展开";
                this.tvFilter.CollapseAll();
            }
        }

        private void btnUse_Click(object sender, EventArgs e)
        {
            if (this.m_OneselfPermissionsOperation.SavePermissions())
            {
                this.btnUse.Enabled = false;
                this.m_Refresh = true;
            }
            else
            {
                SysMessages.Show(this, InformationTip.Fail);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.btnUse.Enabled)
            {
                if (this.m_OneselfPermissionsOperation.SavePermissions())
                {
                    this.m_Refresh = true;
                    this.Close();
                }
                else
                {
                    SysMessages.Show(this, InformationTip.Fail);
                }
            }
            else
            {
                this.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tvData_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            node.SelectedImageIndex = node.ImageIndex;
            if (e.Button == MouseButtons.Right)
            {
                this.tvData.SelectedNode = node;
                this.contextMenuStrip1.Show(Form.MousePosition);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            TreeNode node = this.tvData.SelectedNode;
            switch (node.ImageIndex) {
                case 0:
                    this.tsmiOpen.Visible = true;
                    this.tsmiCannel.Visible = this.tsmiPingBi.CheckOnClick = this.tsmiPingBi.Checked = false;
                    this.tsmiPingBi.Tag = 0;
                    break;
                case 1:
                    this.tsmiCannel.Visible = true;
                    this.tsmiOpen.Visible = this.tsmiPingBi.CheckOnClick = this.tsmiPingBi.Checked = false;
                    this.tsmiPingBi.Tag = 0;
                    break;
                case 2:
                    this.tsmiPingBi.CheckOnClick = this.tsmiPingBi.Checked = true;
                    Boolean vs1 = ((String)node.Tag).Split(',')[2] == "0";
                    this.tsmiOpen.Visible = vs1;
                    this.tsmiCannel.Visible = !vs1;
                    this.tsmiPingBi.Tag = 1;
                    break;
                case 3:
                    this.tsmiPingBi.CheckOnClick = this.tsmiPingBi.Checked = false;
                    Boolean vs2 = ((String)node.Tag).Split(',')[2] == "0";
                    this.tsmiOpen.Visible = vs2;
                    this.tsmiCannel.Visible = !vs2;
                    this.tsmiPingBi.Tag = 0;
                    break;
            }
            if (this.tsmiPingBi.Checked) {
                this.tsmiPingBi.Text = "取消屏蔽";
            }
            else {
                this.tsmiPingBi.Text = "屏蔽";
            }
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            this.tsmiPingBiFileter.Checked = this.tvFilter.SelectedNode.ImageIndex == 1;
            if (this.tsmiPingBiFileter.Checked) {
                this.tsmiPingBiFileter.Text = "取消屏蔽";
            }
            else {
                this.tsmiPingBiFileter.Text = "屏蔽";
            }
        }

        private void tsmiOpen_Click(object sender, EventArgs e)
        {
            TreeNode node = this.tvData.SelectedNode;
            this.SetUsePermissions(node, true, true);
            TreeNodeCollection nodes = node.Nodes;
            Int32 count = nodes.Count;
            if (count > 0)
            {
                if (SysMessages.Show(this, "是否同时开通所有下属权限？", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    while (--count >= 0)
                    {
                        this.SetSunNodesPermissions(nodes, true, true);
                    }
                }
            }
            this.btnUse.Enabled = true;
        }

        private void tsmiCannel_Click(object sender, EventArgs e)
        {
            TreeNode node = this.tvData.SelectedNode;
            this.SetUsePermissions(node, true, false);
            TreeNodeCollection nodes = node.Nodes;
            Int32 count = nodes.Count;
            if (count > 0)
            {
                if (SysMessages.Show(this, "是否同时取消所有下属权限？", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    while (--count >= 0)
                    {
                        this.SetSunNodesPermissions(nodes, true, false);
                    }
                }
            }
            this.btnUse.Enabled = true;
        }

        private void tsmiPingBi_Click(object sender, EventArgs e)
        {
            TreeNode currentNode = this.tvData.SelectedNode;
            if ((Int32)((ToolStripMenuItem)sender).Tag == 0)
            {
                Boolean pass = true;
                if (currentNode.Nodes.Count > 0)
                {
                    pass = SysMessages.Show(this, "屏蔽父级会导致其下属权限失效，是否继续？", MessageBoxButtons.YesNo) == DialogResult.Yes;
                }
                if (pass)
                {
                    TreeNode filterNode = this.FindOtherControlSamePositionNode(currentNode, this.tvFilter);
                    filterNode.ImageIndex = filterNode.SelectedImageIndex = 1;
                    this.SetUsePermissions(currentNode, false, true);
                }
            }
            else
            {
                TreeNode filterNode = this.FindOtherControlSamePositionNode(currentNode, this.tvFilter);
                filterNode.ImageIndex = filterNode.SelectedImageIndex = 0;
                this.SetUsePermissions(currentNode, false, false);
            }
            this.btnUse.Enabled = true;
        }

        private void tvFilter_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            node.SelectedImageIndex = node.ImageIndex;
            if (e.Button == MouseButtons.Right) {
                this.tvFilter.SelectedNode = node;
                this.contextMenuStrip2.Show(Form.MousePosition);
            }
        }

        private void tsmiPingBiFileter_Click(object sender, EventArgs e)
        {
            TreeNode currentNode = this.tvFilter.SelectedNode;
            if (currentNode.ImageIndex == 0)
            {
                Boolean pass = true;
                if (currentNode.Nodes.Count > 0)
                {
                    pass = SysMessages.Show(this, "屏蔽父级会导致其下属权限失效，是否继续？", MessageBoxButtons.YesNo) == DialogResult.Yes;
                }
                if (pass)
                {
                    currentNode.SelectedImageIndex = currentNode.ImageIndex = 1;
                    this.SetUsePermissions(this.FindOtherControlSamePositionNode(currentNode, this.tvData), false, true);
                }
            }
            else
            {
                currentNode.SelectedImageIndex = currentNode.ImageIndex = 0;
                this.SetUsePermissions(this.FindOtherControlSamePositionNode(currentNode, this.tvData), false, false);
            }
            this.btnUse.Enabled = true;
        }

        private void SetPermissions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_Refresh)
            {
                this.DialogResult = DialogResult.Yes;
            }
        }

        private void llBind_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SetInherit.ShowDialogBy(this, this.m_Id, this.m_Name, this.m_PermissionsLoadingType) == DialogResult.Yes)
            {
                this.m_Refresh = true;
                this.Bind();
            }
        }

        private TreeNode FindOtherControlSamePositionNode(TreeNode currentNode, TreeView treeView)
        {
            TreeNode node = currentNode;
            Int32 level = node.Level;
            Int32[] indexs = new Int32[level + 1];
            indexs[level] = node.Index;
            Int32 l = level;
            while (--l >= 0)
            {
                node = node.Parent;
                indexs[l] = node.Index;
            }
            TreeNode node1 = new TreeNode();
            node1 = treeView.Nodes[indexs[0]];
            for (Int32 i = 1; i <= level; i++)
            {
                node1 = node1.Nodes[indexs[i]];
            }
            return node1;
        }

        private void AddNodes(Int32 parentId, DataTable dataTable, TreeNodeCollection treeNodeCollection1, TreeNodeCollection treeNodeCollection2, out Boolean openParent1, out Boolean openParent2)
        {
            openParent1 = openParent2 = false;
            DataTable resultTable = DataTableBuilder.Select(dataTable, "ParentID=" + parentId);
            DataRowCollection rows = resultTable.Rows;
            Int32 length = rows.Count;
            for (Int32 i = 0; i < length; i++)
            {
                DataRow row = rows[i];
                String name = (String)row["ModuleName"];
                TreeNode node1 = treeNodeCollection1.Add(name);
                TreeNode node2 = treeNodeCollection2.Add(name);

                Int32 keyIndex = (Int32)row["KeyIndex"];
                Object bitLocation = row["BitLocation"];
                node1.Tag = node2.Tag = keyIndex + "," + bitLocation;
                Int32 location = (Int32)bitLocation;

                Int32 imageIndex = -1;
                BinarySet originalBinarySet;
                if (this.m_OneselfPermissionsOperation.VerifyPermissionByKeyIndex(keyIndex, out originalBinarySet))
                {
                    BinarySet data = PermissionsOperation.Default;
                    data.Inject(originalBinarySet.Data, false);
                    if (data.VerifyByOrdinal(location))
                    {
                        if (!openParent1)
                        {
                            openParent1 = true;
                        }
                        node1.Tag += ",1";
                        imageIndex = 1;
                    }
                    else
                    {
                        node1.Tag += ",0";
                        imageIndex = 0;
                    }
                    BinarySet filter = PermissionsOperation.Default;
                    filter.Inject(originalBinarySet.Filter, false);
                    if (filter.VerifyByOrdinal(location))
                    {
                        if (!openParent1)
                        {
                            openParent1 = true;
                        }
                        if (!openParent2)
                        {
                            openParent2 = true;
                        }
                        node2.ImageIndex = node2.SelectedImageIndex = 1;
                        imageIndex = 2;
                    } 
                    else
                    {
                        node2.ImageIndex = node2.SelectedImageIndex = 0;
                    }
                }
                else
                {
                    node1.Tag += ",0";
                    node2.ImageIndex = node2.SelectedImageIndex = 0;
                }

                if (this.m_BindingsPermissionsOperation.Verify(keyIndex, location))
                {
                    if (!openParent1)
                    {
                        openParent1 = true;
                    }
                    node1.Tag += ",1";
                    if (imageIndex != 2)
                    {
                        imageIndex = 3;
                    }
                }
                else
                {
                    node1.Tag += ",0";
                }
                if (imageIndex > -1)
                {
                    node1.ImageIndex = node1.SelectedImageIndex = imageIndex;
                }
                else
                {
                    node1.ImageIndex = node1.SelectedImageIndex = 0;
                }

                Boolean open1;
                Boolean open2;
                this.AddNodes((Int32)row["ModuleID"], dataTable, node1.Nodes, node2.Nodes, out  open1, out open2);
                if (open1)
                {
                    node1.Expand();
                    openParent1 = true;
                }
                if (open2)
                {
                    node2.Expand();
                    openParent2 = true;
                }
            }
        }

        private void SetUsePermissions(TreeNode node, Boolean isData, Boolean isChecked)
        {
            String tag = (String)node.Tag;
            Int32[] values = (Int32[])tag.Split(',').ChangeType(typeof(Int32));
            if (isChecked)
            {
                if (isData)
                {
                    this.m_OneselfPermissionsOperation.InjectByKeyIndex(values[0], values[1]);
                    if (node.ImageIndex == 0)
                    {
                        node.SelectedImageIndex = node.ImageIndex = 1;
                    }
                    values[2] = 1;
                    node.Tag = values.ToString(",");
                }
                else
                {
                    this.m_OneselfPermissionsOperation.InjectFilterByKeyIndex(values[0], values[1]);
                    node.SelectedImageIndex = node.ImageIndex = 2;
                }
            }
            else
            {
                if (isData)
                {
                    this.m_OneselfPermissionsOperation.RemoveByKeyIndex(values[0], values[1]);
                    if (node.ImageIndex == 1)
                    {
                        node.SelectedImageIndex = node.ImageIndex = 0;
                    }
                    values[2] = 0;
                    node.Tag = values.ToString(",");
                }
                else
                {
                    this.m_OneselfPermissionsOperation.RemoveFilterByKeyIndex(values[0], values[1]);
                    node.SelectedImageIndex = node.ImageIndex = values[3] == 1 ? 3 : values[2];
                }
            }
        }

        private void SetSunNodesPermissions(TreeNodeCollection nodes, Boolean isData, Boolean isChecked)
        {
            Int32 count = nodes.Count;
            if (count > 0)
            {
                while (--count >= 0)
                {
                    TreeNode node = nodes[count];
                    this.SetUsePermissions(node, isData, isChecked);
                    this.SetSunNodesPermissions(node.Nodes, isData, isChecked);
                }
            }
        }

        private void Bind()
        {
            this.m_OneselfPermissionsOperation = new PermissionsOperation(this.m_Id, this.m_PermissionsLoadingType, true, false);
            this.m_BindingsPermissionsOperation = new PermissionsOperation(this.m_Id, this.m_PermissionsLoadingType, false, true);

            this.tvData.Nodes.Clear();
            this.tvFilter.Nodes.Clear();
            DataTable dataTable = Helpers.GetData<Module>(SqlWhereBuilder.None);
            if (dataTable.Rows.Count > 0)
            {
                Boolean open1;
                Boolean open2;
                this.AddNodes(0, dataTable, this.tvData.Nodes, this.tvFilter.Nodes, out open1, out open2);
            }
            this.tvData.Sort();
            this.tvFilter.Sort();
        }
    }

}
