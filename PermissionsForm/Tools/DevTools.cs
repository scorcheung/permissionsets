﻿using System;

namespace Permissions.Tools
{
    internal static class DevTools
    {
        public static string SafeTrimSql(string databaseName)
        {
            return (databaseName != null ? databaseName.Replace("'", "''") : null);
        }
        /// <summary>
        /// 获取无符号的大写GUID
        /// </summary>
        /// <returns></returns>
        public static string GetUpperGuidNoSymbol()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }

    }
}
