﻿using System;
using System.Windows.Forms;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Database.Tables;
using Permissions.Helper;
using Permissions.Tools;

namespace Permissions
{
    internal sealed partial class RolesEdit : Form
    {
        private Boolean m_Insert;
        private String m_Id;

        private RolesEdit()
        {
            this.InitializeComponent();
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow)
        {
            return new RolesEdit()
            {
                m_Insert = true
            }.ShowDialog(parentWindow);
        }

        public static DialogResult ShowDialogBy(IWin32Window parentWindow, String id)
        {
            return new RolesEdit()
            {
                m_Insert = false,
                m_Id = id
            }.ShowDialog(parentWindow);
        }

        private void RolesEditt_Load(object sender, System.EventArgs e)
        {
            KeyValues values = null;
            if (!this.m_Insert) {
                Roles roles = Roles.SelectSingleOnly(SqlWhereBuilder.MSSQL.Append("ID", this.m_Id));
                if (roles != null) {
                    this.txtName.Text = roles.RoleName;
                    this.txtRemark.Text = roles.Description;
                }
                values = new KeyValues();
                foreach (RolesInfo roleBindings in RolesInfo.Select(SqlWhereBuilder.MSSQL.Append("RoleID", this.m_Id))) {
                    values.Add(roleBindings.KeyIndex, new Int64[] { roleBindings.Data, roleBindings.Filter });
                }

            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            string name = this.txtName.Text.Trim();
            if (name.Length <= 0) {
                SysMessages.Show(this, "请填写角色名称。", MessageBoxButtons.OK);
                this.txtName.Focus();
            }
            else if (name.IndexOf('(') >= 0) {
                SysMessages.Show(this, "名称不允许有\"(\"符号。", MessageBoxButtons.OK);
                this.txtName.Focus();
            }
            else {

                SqlWhereBuilder sqlWhere = SqlWhereBuilder.MSSQL.Append("RoleName", name);
                if (!this.m_Insert) {
                    sqlWhere.And("ID", this.m_Id, SqlWhereBuilder.ComparisonSymbolTypes.Inequality);
                }
                if (Roles.Exists(sqlWhere)) {
                    SysMessages.Show(this, "名称已经存在，请填写其它名称。", MessageBoxButtons.OK);
                    this.txtName.Focus();
                }
                else {
                    Roles roles = null;
                    if (this.m_Insert) {
                        roles = Roles.New;
                        roles.ID = DevTools.GetUpperGuidNoSymbol();
                    }
                    else {
                        roles = Roles.SelectSingleOnly(SqlWhereBuilder.MSSQL.Append("ID", this.m_Id));
                    }
                    if (roles != null) {
                        roles.RoleName = name;
                        roles.Description = this.txtRemark.Text;
                        if (this.m_Insert) {
                            roles.Insert();
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }
                        else {
                            if (roles.Update() <= 0) {
                                SysMessages.Show(this, InformationTip.Fail);
                            }
                            else {
                                this.DialogResult = DialogResult.OK;
                                this.Close();
                            }
                        }
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
