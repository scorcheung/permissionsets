﻿namespace Permissions
{
    partial class SetPermissions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetPermissions));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.llOpen1 = new System.Windows.Forms.LinkLabel();
            this.tvData = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.llOpen2 = new System.Windows.Forms.LinkLabel();
            this.tvFilter = new System.Windows.Forms.TreeView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.llBind = new System.Windows.Forms.LinkLabel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUse = new System.Windows.Forms.Button();
            this.tsmiData = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFilter = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCannel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLine = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiPingBi = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiPingBiFileter = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.llOpen1);
            this.groupBox1.Controls.Add(this.tvData);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 475);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "允许";
            // 
            // llOpen1
            // 
            this.llOpen1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llOpen1.AutoSize = true;
            this.llOpen1.Location = new System.Drawing.Point(241, 13);
            this.llOpen1.Name = "llOpen1";
            this.llOpen1.Size = new System.Drawing.Size(53, 12);
            this.llOpen1.TabIndex = 10;
            this.llOpen1.TabStop = true;
            this.llOpen1.Tag = "";
            this.llOpen1.Text = "全部展开";
            this.llOpen1.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llOpen1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llOpen1_LinkClicked);
            // 
            // tvData
            // 
            this.tvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvData.ImageIndex = 0;
            this.tvData.ImageList = this.imageList1;
            this.tvData.ItemHeight = 18;
            this.tvData.Location = new System.Drawing.Point(3, 28);
            this.tvData.Name = "tvData";
            this.tvData.SelectedImageIndex = 0;
            this.tvData.Size = new System.Drawing.Size(294, 441);
            this.tvData.TabIndex = 1;
            this.tvData.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvData_NodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "default.gif");
            this.imageList1.Images.SetKeyName(1, "yes.gif");
            this.imageList1.Images.SetKeyName(2, "no.gif");
            this.imageList1.Images.SetKeyName(3, "inherit.gif");
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.llOpen2);
            this.groupBox2.Controls.Add(this.tvFilter);
            this.groupBox2.Location = new System.Drawing.Point(320, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 475);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "拒绝";
            // 
            // llOpen2
            // 
            this.llOpen2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.llOpen2.AutoSize = true;
            this.llOpen2.Location = new System.Drawing.Point(241, 13);
            this.llOpen2.Name = "llOpen2";
            this.llOpen2.Size = new System.Drawing.Size(53, 12);
            this.llOpen2.TabIndex = 12;
            this.llOpen2.TabStop = true;
            this.llOpen2.Tag = "";
            this.llOpen2.Text = "全部展开";
            this.llOpen2.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llOpen2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llOpen2_LinkClicked);
            // 
            // tvFilter
            // 
            this.tvFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvFilter.ImageIndex = 0;
            this.tvFilter.ImageList = this.imageList2;
            this.tvFilter.ItemHeight = 18;
            this.tvFilter.Location = new System.Drawing.Point(3, 28);
            this.tvFilter.Name = "tvFilter";
            this.tvFilter.SelectedImageIndex = 0;
            this.tvFilter.Size = new System.Drawing.Size(294, 441);
            this.tvFilter.TabIndex = 1;
            this.tvFilter.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvFilter_NodeMouseClick);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "default.gif");
            this.imageList2.Images.SetKeyName(1, "duihao.gif");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.llBind);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnUse);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 521);
            this.panel1.TabIndex = 3;
            // 
            // llBind
            // 
            this.llBind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llBind.AutoSize = true;
            this.llBind.Location = new System.Drawing.Point(11, 496);
            this.llBind.Name = "llBind";
            this.llBind.Size = new System.Drawing.Size(77, 12);
            this.llBind.TabIndex = 10;
            this.llBind.TabStop = true;
            this.llBind.Text = "查看继承关系";
            this.llBind.VisitedLinkColor = System.Drawing.Color.Blue;
            this.llBind.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llBind_LinkClicked);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Location = new System.Drawing.Point(464, 491);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(545, 491);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUse
            // 
            this.btnUse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUse.Enabled = false;
            this.btnUse.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUse.Location = new System.Drawing.Point(383, 491);
            this.btnUse.Name = "btnUse";
            this.btnUse.Size = new System.Drawing.Size(75, 23);
            this.btnUse.TabIndex = 3;
            this.btnUse.Text = "应用";
            this.btnUse.UseVisualStyleBackColor = true;
            this.btnUse.Click += new System.EventHandler(this.btnUse_Click);
            // 
            // tsmiData
            // 
            this.tsmiData.Name = "tsmiData";
            this.tsmiData.Size = new System.Drawing.Size(32, 19);
            // 
            // tsmiFilter
            // 
            this.tsmiFilter.Name = "tsmiFilter";
            this.tsmiFilter.Size = new System.Drawing.Size(32, 19);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOpen,
            this.tsmiCannel,
            this.tsmiLine,
            this.tsmiPingBi});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(121, 76);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // tsmiOpen
            // 
            this.tsmiOpen.Image = global::Permissions.Properties.Resources.yes;
            this.tsmiOpen.Name = "tsmiOpen";
            this.tsmiOpen.Size = new System.Drawing.Size(120, 22);
            this.tsmiOpen.Text = "开通(&S)";
            this.tsmiOpen.Click += new System.EventHandler(this.tsmiOpen_Click);
            // 
            // tsmiCannel
            // 
            this.tsmiCannel.Image = global::Permissions.Properties.Resources.custom;
            this.tsmiCannel.Name = "tsmiCannel";
            this.tsmiCannel.Size = new System.Drawing.Size(120, 22);
            this.tsmiCannel.Text = "取消(&R)";
            this.tsmiCannel.Click += new System.EventHandler(this.tsmiCannel_Click);
            // 
            // tsmiLine
            // 
            this.tsmiLine.Name = "tsmiLine";
            this.tsmiLine.Size = new System.Drawing.Size(117, 6);
            // 
            // tsmiPingBi
            // 
            this.tsmiPingBi.CheckOnClick = true;
            this.tsmiPingBi.Image = global::Permissions.Properties.Resources.no;
            this.tsmiPingBi.Name = "tsmiPingBi";
            this.tsmiPingBi.Size = new System.Drawing.Size(120, 22);
            this.tsmiPingBi.Tag = "0";
            this.tsmiPingBi.Text = "屏蔽(&W)";
            this.tsmiPingBi.Click += new System.EventHandler(this.tsmiPingBi_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiPingBiFileter});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(101, 26);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // tsmiPingBiFileter
            // 
            this.tsmiPingBiFileter.CheckOnClick = true;
            this.tsmiPingBiFileter.Image = ((System.Drawing.Image)(resources.GetObject("tsmiPingBiFileter.Image")));
            this.tsmiPingBiFileter.Name = "tsmiPingBiFileter";
            this.tsmiPingBiFileter.Size = new System.Drawing.Size(100, 22);
            this.tsmiPingBiFileter.Text = "屏蔽";
            this.tsmiPingBiFileter.Click += new System.EventHandler(this.tsmiPingBiFileter_Click);
            // 
            // SetPermissions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 521);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(646, 443);
            this.Name = "SetPermissions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "权限设定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetPermissions_FormClosing);
            this.Load += new System.EventHandler(this.SetRolesPermissions_Load);
            this.Resize += new System.EventHandler(this.SetRolesPermissions_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView tvData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TreeView tvFilter;
        private System.Windows.Forms.Button btnUse;
        private System.Windows.Forms.ToolStripMenuItem tsmiData;
        private System.Windows.Forms.ToolStripMenuItem tsmiFilter;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiCannel;
        private System.Windows.Forms.ToolStripMenuItem tsmiOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmiPingBi;
        private System.Windows.Forms.ToolStripSeparator tsmiLine;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tsmiPingBiFileter;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.LinkLabel llOpen1;
        private System.Windows.Forms.LinkLabel llOpen2;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.LinkLabel llBind;

    }
}