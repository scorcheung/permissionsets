﻿using System;
using System.Windows.Forms;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using Permissions.Database;
using Permissions.Database.Tables;

namespace Permissions.Application
{
    public static class OperationAdapter
    {
        /// <summary>
        /// 指定用于处理和显示权限的全局数据库连接字符串信息。
        /// </summary>
        public static Object DatabaseConnectionSelection
        {
            set
            {
                Helpers.DatabaseConnectionSelection = value;
            }
        }

        /// <summary>
        /// 显示角色管理窗体
        /// </summary>
        public static void ShowDialogRolesForm(IWin32Window parentWindow)
        {
            RolesSys.ShowDialogBy(parentWindow);
        }

        /// <summary>
        /// 显示用户管理窗体
        /// </summary>
        public static void ShowDialogUsersForm(IWin32Window parentWindow)
        {
            UsersSys.ShowDialogBy(parentWindow);
        }

        /// <summary>
        /// 清除用户权限
        /// </summary>
        /// <param name="userID">用户ID</param>
        public static Boolean ClearUserPermissions(String userID)
        {
            return DbDatabase.ExecTransactions(delegate()
            {
                try
                {
                    UserBindings.DeleteBy(SqlWhereBuilder.MSSQL.Append("UserID", userID));
                    UsersInfo.DeleteBy(SqlWhereBuilder.MSSQL.Append("UserID", userID));
                    return true;
                }
                catch { }
                return false;
            });
        }
    }
}
